from flask import Flask
import os
app = Flask(__name__)

@app.route("/")
def hello_www():
    return "<strong> My name is Hatem </strong>"

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=os.environ.get('PORT'))